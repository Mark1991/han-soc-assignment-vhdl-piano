--------------------------------------------------------------------
--! \file      showkey.vhd
--! \date      see top of 'Version History'
--! \brief     Showkey
--! \author    Remko Welling (WLGRW) remko.welling@han.nl
--! \copyright HAN TF ELT/ESE Arnhem 
--!
--! Version History:
--! ----------------
--!
--! Nr:    |Date:      |Author: |Remarks:
--! -------|-----------|--------|-----------------------------------
--! 001    |20-3-2020  |WLGRW   |Inital version
--! 002    |6-7-2020   |WLGRW   |Added a todo not to modify the header of the file to represent teh students that worked on the file.
--!
--! \todo MODIFY THE HEADER OF THIS FILE TO REPRESENT THE NAMES OF THE STUDENTS WORKING ON IT.
--!
--! # Opdracht 1: Deelopdracht showkey
--!
--! showkey will convert any PS2 Keyboard formatted serial received byte 
--! in to a 8-byte parallel word on port 'scancode' When a correct byte 
--! is presented at 'scancode' byte_read will present a latch signal 
--! eg. '0,1'
--! 
--! \verbatim
--!                            +-----------------+
--!                            |                 |
--!                   reset -->|                 |--> dig0[7..0]
--!                            |                 |
--!                            |                 |--> dig1[7..0]
--! kbclock (50 MHz domain) -->|     showkey     |
--!                            |                 |--> scancode[7..0]
--!                            |                 |
--!  kbdata (50 MHz domain) -->|                 |--> byte_read
--!                            |                 |
--!                            +-----------------+
--!
--! Figure: component showkey
--!
--! \endverbatim
--!
--! \todo Complete documentation
--!
--------------------------------------------------------------------
LIBRARY ieee;
USE     ieee.std_logic_1164.all,
        ieee.numeric_std.all;    -- For unsigned variable
--------------------------------------------------------------------
ENTITY showkey IS
   GENERIC(
      clk_freq              : INTEGER := 50_000_000; --system clock frequency in Hz
      showkey_counter_size  : INTEGER := 8);          --set such that (2^size)/clk_freq = 5us (size = 8 for 50MHz)
      
      
   PORT(
      clk                   : IN  STD_LOGIC;                     -- system clock
      reset                 : IN  std_logic;                     -- reset signal active low '0'
      kbclock               : IN  std_logic;                     -- clock from keyboard in 50 MHz domain
      kbdata                : IN  std_logic;                     -- serial data from the keyboard in 50 MHz domain
      dig0,                                                      -- show key pressed on display in hex (MSB)
      dig1                  : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);  -- show key pressed on display in hex (LSB)
      scancode              : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);  -- received byte
      byte_read             : OUT std_logic                      -- '1' if byte received
      
 
   );
END showkey;
--------------------------------------------------------------------
ARCHITECTURE LogicFunction OF showkey IS
SIGNAL counter    : INTEGER RANGE 0 to 10;
SIGNAL PS2_date   : STD_LOGIC_VECTOR(7 downto 0); -- LSB > MSB
SIGNAL PS2_word   : STD_LOGIC_VECTOR (10 DOWNTO 1);
SIGNAL kbdata_int : STD_LOGIC;
SIGNAL kbclock_int: STD_LOGIC;
SIGNAL count_idle : INTEGER;
SIGNAL error      : STD_LOGIC;

BEGIN 

  --synchronizer flip-flops
  PROCESS(kbclock,reset) -- Based on flow chart page 26 assigment reader
  BEGIN

  IF  (reset = '0') THEN
      dig0     <= x"00";
      dig1     <= x"00";
      PS2_date <= x"00";
      counter  <= 0;
      byte_read<= '0';
     ELSIF (falling_edge(kbclock)) THEN
         IF (counter = 0) THEN      -- start bit
         dig0(7 downto 0)        <= ps2_date (7 downto 0);
         byte_read   <= '0';-- as soon as the first bit from the keyboard is recieved by showkey port read_byte drops form '1' to 
         counter     <= (counter + 1);
     
     ELSIF (counter < 9) THEN -- data bits
         ps2_date(counter-1) <= kbdata;
         counter              <= (counter + 1);
     ELSIF (counter = 10) THEN -- stop bits
     byte_read <= '1'; --scancode is complete, byte_read will shift to 1
     dig1(7 downto 0)      <= ps2_date (7 downto 0); -- msb > LSB shift
     scancode(7 downto 0)  <= ps2_date (7 downto 0); -- msb > LSB shift 
     counter <= 0;
     END IF;
     END IF;
     END PROCESS;

--  --showkey PS2 input signals
--  showkey_kbclock: showkey
--    GENERIC MAP(counter_size => showkey_counter_size)
--    PORT MAP(clk => clk, button => sync_ffs(0), result => kbclock_int);
--  showkey_kbdata: showkey
--    GENERIC MAP(counter_size => showkey_counter_size)
--    PORT MAP(clk => clk, button => sync_ffs(1), result => kbdata_int);

  --input PS2 data
  PROCESS(kbclock_int)
  BEGIN
    IF(kbclock_int'EVENT AND kbclock_int = '0') THEN    --falling edge of PS2 clock
      ps2_word <= kbdata_int & ps2_word(10 DOWNTO 1);   --shift in PS2 data bit
    END IF;
  END PROCESS;
    
  

  --determine if PS2 port is idle (i.e. last transaction is finished) and output result
  PROCESS(clk)
  BEGIN
    IF(clk'event AND clk= '1') THEN               --rising edge of system clock
    
      IF(kbclock_int = '0') THEN                   --low PS2 clock, PS/2 is active
        count_idle <= 0;                           --reset idle counter
      ELSIF(count_idle /= clk_freq/18_000) THEN    --PS2 clock has been high less than a half clock period (<55us)
          count_idle <= count_idle + 1;            --continue counting
      END IF;
      
      IF(count_idle = clk_freq/18_000 AND error = '0') THEN  --idle threshold reached and no errors detected
--        byte_read <= '1';                                    --set flag that new PS/2 code is available
        scancode <= ps2_word(8 DOWNTO 1);                    --output new PS/2 code
--      ELSE                                                   --PS/2 port active or error detected
--        byte_read <= '0';                                    --set flag that PS/2 transaction is in progress
      END IF;
      
    END IF;
  END PROCESS;
  
      -- Add here the VHDL code for showkey

end LogicFunction;
--------------------------------------------------------------------

