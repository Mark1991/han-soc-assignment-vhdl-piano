--------------------------------------------------------------------
--! \file      clock_generator.vhd
--! \date      see top of 'Version History'
--! \brief     Clock generator 
--! \author    Remko Welling (WLGRW) remko.welling@han.nl
--! \copyright HAN TF ELT/ESE Arnhem 
--!
--! Version History:
--! ----------------
--!
--! Nr:    |Date:      |Author: |Remarks:
--! -------|-----------|--------|-----------------------------------
--! 001    |24-3-2015  |WLGRW   |Inital version
--! 002    |13-2-2020  |WLGRW   |Update to MAX10 on DE10-Lite board
--! 003    |6-4-2020   |WLGRW   |Cleanup for assignment
--! 004    |6-7-2020   |WLGRW   |Added a todo not to modify the header of the file to represent teh students that worked on the file.
--!
--! \todo MODIFY THE HEADER OF THIS FILE TO REPRESENT THE NAMES OF THE STUDENTS WORKING ON IT.
--!
--! # Assignment  3: clock_generator
--!
--! This clock_generator wil devide the system clock of 50 MHz 1, 1/2, 1/4, 1/8, 1/16, 1/32 and 1/64 times
--! This will produce frequencies of: 50MHz, 25 MHz, 12.5MHz, 6,25MHz, 3,125MHz, 1,5625MHz or 0,78125MHz.
--! 
--! 
--------------------------------------------------------------------
LIBRARY ieee;
USE     ieee.STD_LOGIC_1164.all;
USE     ieee.numeric_std.all;
--------------------------------------------------------------------
ENTITY clock_generator IS

   GENERIC(
      N: INTEGER := 6
   );
   
   PORT(
      reset   : in  STD_LOGIC;                    -- Reset active is '0'
      clk     : in  STD_LOGIC;                    -- 50MHz clock
      key     : in  STD_LOGIC_VECTOR(7 DOWNTO 0);
      clk_div : out STD_LOGIC
   );
END clock_generator;
--------------------------------------------------------------------
ARCHITECTURE LogicFunction OF clock_generator IS
--! add a "typedef" for enum state_type.
   TYPE state_type is (
   state_0, -- clock 50 MHZ
   state_1, -- clock 25 MHZ
   state_2, -- clock 12.5 MHZ
   state_3, -- clock 6.25 MHZ
   state_4, -- clock 3.125 MHZ
   state_5, -- clock 1.562 MHZ
   state_6  -- clock 0.781 MHZ
   );

--! add SIGNALS 
   SIGNAL divider_output : STD_LOGIC_VECTOR(N DOWNTO 0); -- for counter
   SIGNAL selector : INTEGER RANGE 0 TO 2 ** N := 0;     -- for counter


--! add a signal of type state_type for the FSM
   SIGNAL current_state, 
   next_state : state_type;
   
BEGIN


--! #### clock devider based on a integer counter: #### 
--! The signal divider_output receives the integer
--! Per line on the vector a devided signal of the clock is avaialable
--! lsb is devide by 1, msb is devide by 64

   devider : PROCESS (clk, reset)
      VARIABLE counter : INTEGER RANGE 0 TO 2 ** N := 0;
      
   BEGIN
      
   
      IF reset = '0' THEN                   -- Async reset
      
         counter := 0;                      -- set counter to 0
         divider_output <= (OTHERS => '0'); -- set counter to 0 
         
      ELSIF rising_edge(clk) THEN           -- on clock edge
      
         IF (counter < (2 ** N)) THEN       -- as long as the counter is below 2 power N
           counter := counter + 1;          -- increment counter
         ELSE                               -- as the counter reached 2 power N
           counter := 0;                    -- reset counter to 0
         END IF;
         
      END IF;                               -- put result of counter on signal
      
      divider_output <= STD_LOGIC_VECTOR(to_unsigned(counter, divider_output'length));
      
     
   END PROCESS;
   
--      selector <= to_integer(unsigned(clk_sel));
   
   WITH selector SELECT 
      clk_div <= divider_output(0) WHEN 0,
                 divider_output(1) WHEN 1,
                 divider_output(2) WHEN 2,
                 divider_output(3) WHEN 3,
                 divider_output(4) WHEN 4,
                 divider_output(5) WHEN 5,
                 divider_output(6) WHEN 6,
                 divider_output(3) WHEN OTHERS;

--! #### state_decoder ####
--! this PROCESS is PROCESSing state changes each clk and executing async reset.
--! the PROCESS will handle coninous pressing a key as well.

   state_decoder: PROCESS (clk, reset, selector) is -- PROCESS watching reset and system clock
   BEGIN
   
   --! reset all output signals of FSM 
   --! set FSM to inital state 
   IF reset = '0' THEN -- Reset (async).
      current_state <= state_3;
   ELSIF rising_edge(clk) THEN 
      current_state <= next_state; 

      
   
   END IF;   
   
     
   END PROCESS;                  -- END PROCESS input_decoder

--! #### input_decoder #### 
--! this PROCESS contains the tests and conditions for each state

   input_decoder : PROCESS ( key, current_state ) -- add state SIGNAL to watch-list
   BEGIN
      
   CASE current_state is
      WHEN state_0 =>
         IF (key = "01011010") THEN     -- key pressing z
            next_state <= state_1; 
         ELSIF (key = "01010010") THEN  -- key pressing A
            next_state <= current_state; --! prevent inferring latches
         ELSE
            next_state <= current_state;
         END IF; 
 
      WHEN state_1 =>
         IF (key = "01011010") THEN     -- key pressing z
            next_state <= state_2; 
         ELSIF (key = "01010010") THEN  -- key pressing A
            next_state <= state_0; --! prevent inferring latches
         ELSE
            next_state <= current_state;
         END IF;  
 
       WHEN state_2 =>
         IF (key = "01011010") THEN     -- key pressing z
            next_state <= state_3; 
         ELSIF (key = "01010010") THEN  -- key pressing A
            next_state <= state_1; --! prevent inferring latches
         ELSE
            next_state <= current_state;
         END IF;
 
       WHEN state_3 =>
         IF (key = "01011010") THEN     -- key pressing z
            next_state <= state_4; 
         ELSIF (key = "01010010") THEN  -- key pressing A
            next_state <= state_2; --! prevent inferring latches
         ELSE
            next_state <= current_state;
         END IF;
         
      WHEN state_4 =>
         IF (key = "01011010") THEN     -- key pressing z
            next_state <= state_5; 
         ELSIF (key = "01010010") THEN  -- key pressing A
            next_state <= state_3; --! prevent inferring latches
         ELSE
            next_state <= current_state;
         END IF;
 
       WHEN state_5 =>
         IF (key = "01011010") THEN     -- key pressing z
            next_state <= state_6; 
         ELSIF (key = "01010010") THEN  -- key pressing A
            next_state <= state_4; --! prevent inferring latches
         ELSE
            next_state <= current_state;
         END IF;
 
        WHEN state_6 =>
         IF (key = "01010010") THEN  -- key pressing A
            next_state <= state_5; --! prevent inferring latches
         ELSE
            next_state <= current_state;
         END IF;
         
        WHEN OTHERS =>
        next_state <= current_state;
         
       END CASE;
     
   END PROCESS;
      
--! #### state_decoder ####
--! this PROCESS is performing actions that apply for each state
--! selector logic: select the divided clock signal from the clock devider.

   output_decoder : PROCESS (clk) -- add divider_output SIGNAL to watch-list
   BEGIN   
      CASE current_state IS 
         WHEN state_0 => 
            selector <= 0;
         WHEN state_1 => 
            selector <= 1;
         WHEN state_2 => 
            selector <= 2;
         WHEN state_3 => 
            selector <= 3;
         WHEN state_4 => 
            selector <= 4;
         WHEN state_5 => 
            selector <= 5;
         WHEN state_6 => 
            selector <= 6;
         WHEN OTHERS => 
            selector <= 3;
       END CASE;
     
   END PROCESS;
   
END LogicFunction;
--------------------------------------------------------------------