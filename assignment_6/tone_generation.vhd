--------------------------------------------------------------------
--! \file      tone_generation.vhd
--! \date      see top of 'Version History'
--! \brief     combination of components to tone_generation 
--! \author    Remko Welling (WLGRW) remko.welling@han.nl
--! \copyright HAN TF ELT/ESE Arnhem 
--!
--! Version History:
--! ----------------
--!
--! Nr:    |Date:      |Author: |Remarks:
--! -------|-----------|--------|-----------------------------------
--! 001    |22-3-2015  |WLGRW   |Inital version
--! 002    |13-2-2020  |WLGRW   |Update to MAX10 on DE10-Lite board
--! 003    |7-4-2020   |WLGRW   |Modification for use as assignment
--! 004    |6-7-2020   |WLGRW   |Added a todo not to modify the header of the file to represent teh students that worked on the file.
--!
--! \todo MODIFY THE HEADER OF THIS FILE TO REPRESENT THE NAMES OF THE STUDENTS WORKING ON IT.
--! 
--! # assignment 6: tone_generation
--!
--! tone_generation combines the following components:
--!  - clock_domain_crossing
--!  - showkey
--!  - constantkey
--! 
--!
--------------------------------------------------------------------
LIBRARY ieee;
USE     ieee.STD_LOGIC_1164.all;
USE     ieee.STD_LOGIC_arith.all;
USE     ieee.STD_LOGIC_unsigned.all;
--------------------------------------------------------------------
ENTITY tone_generation IS
   PORT(
      clk     : IN  STD_LOGIC;                     -- 50 MHz clock.
      reset   : IN  STD_LOGIC;                     -- reset '0' active
      key     : IN  STD_LOGIC_VECTOR(7 downto 0);  -- kex character received from keyboard
      audiol,
      audior  : OUT STD_LOGIC                      -- Audio output
   );
END tone_generation;
--------------------------------------------------------------------
ARCHITECTURE tone_generation_struct OF tone_generation IS
   
   SIGNAL   c_clk_div     :  STD_LOGIC;
   SIGNAL   c_clk_dev     :  STD_LOGIC; 
   SIGNAL   C_pulselength :  integer RANGE 0 TO 131071;
   
BEGIN
   
 L_clock_generator:ENTITY work.clock_generator PORT MAP(

 reset => reset,
 clk => clk, 
 key => key,
 clk_div => C_clk_dev
 ); 
 

 L_key_2_pulselength: ENTITY work.key2pulselength PORT MAP(
 reset   => reset,     
 key     => key,
 pulselength => C_pulselength
 );


 L_pulselength_2_audio: ENTITY work.pulselength2audio PORT MAP( 
 reset      => reset,    
 clk_dev    => C_clk_div,
 pulslength => C_pulselength,
 audiol     => audiol,
 audior     => audior
 ); 
   
END tone_generation_struct;
--------------------------------------------------------------------
