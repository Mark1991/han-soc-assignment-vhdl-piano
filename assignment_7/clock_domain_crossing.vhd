--       Naam 
--      :  Joost Kuijpers 626317
--      :  Marvin Damman  616918
--      :  Mark Janssen   616931
--------------------------------------------------------------------
LIBRARY ieee;
USE     ieee.STD_LOGIC_1164.all;
--------------------------------------------------------------------
ENTITY clock_domain_crossing IS
   PORT(
      reset       : in  STD_LOGIC; --! reset signal active low '0'
      clk         : in  STD_LOGIC; --! 50 MHz clock from 50 MHz domain
      kbclock     : in  STD_LOGIC; --! clock from keyboard in 20 kHz domain
      kbdata      : in  STD_LOGIC; --! serial from keyboard in 20 kHz domain
      kbclocksync : out STD_LOGIC; --! clock from keyboard in 50 Mhz kHz domain
      kbdatasync  : out STD_LOGIC  --! serial data from keyboard in 50 MHz domain
   );
END clock_domain_crossing;
--------------------------------------------------------------------
ARCHITECTURE LogicFunction OF clock_domain_crossing IS

   COMPONENT flipflop PORT(
      D, 
      clk, 
      reset : in  STD_LOGIC; 
      Q     : out STD_LOGIC
   ); 
   END COMPONENT;

   SIGNAL kbdatasignal, 
          kbclocksignal : STD_LOGIC; --! SIGNALs to interconnect flipflops

BEGIN
   -- component instantiations
   c0: flipflop PORT MAP (kbdata,        clk, reset, kbdatasignal );
   c1: flipflop PORT MAP (kbdatasignal,  clk, reset, kbdatasync );
   c2: flipflop PORT MAP (kbclock,       clk, reset, kbclocksignal );
   c3: flipflop PORT MAP (kbclocksignal, clk, reset, kbclocksync );
      
END LogicFunction;

--! \brief ENTITY and ARCHITECTURE of component flipflop
--! This componant implements a single flipflop.
--------------------------------------------------------------------
LIBRARY ieee;
USE     ieee.STD_LOGIC_1164.all;
--------------------------------------------------------------------
ENTITY flipflop IS --! generic function of a flipflop1 
   PORT(
      D     : in  STD_LOGIC;
      clk   : in  STD_LOGIC;
      reset : in  STD_LOGIC;
      Q     : out STD_LOGIC
   );
END flipflop;
--------------------------------------------------------------------
ARCHITECTURE Logic OF flipflop IS
BEGIN

   flipflop_process: PROCESS (clk, reset) IS
   BEGIN
   
      IF reset = '0' THEN         -- Reset (async).
         Q <= '0';                -- reset Q
      ELSIF rising_edge(clk) THEN -- at clock:
         Q <= D;                  -- copy D to Q
      END IF;                     -- end if of watchlist signals

   END PROCESS;                   -- end process flipflop1_process

END Logic;                        -- end architecture
--------------------------------------------------------------------