--       Naam 
--      :  Joost Kuijpers 626317
--      :  Marvin Damman  616918
--      :  Mark Janssen   616931
--------------------------------------------------------------------
LIBRARY ieee;
USE     ieee.STD_LOGIC_1164.all;
--------------------------------------------------------------------
ENTITY constantkey IS
   PORT (
      reset     : IN  STD_LOGIC;                    --! reset signal active low '0'
      clk       : IN  STD_LOGIC;                    --! clock (50 MHz)
      scancode  : IN  STD_LOGIC_VECTOR(7 DOWNTO 0); --! received byte
      byte_read : IN  STD_LOGIC;                    --! Set if byte is received 
      key       : OUT STD_LOGIC_VECTOR(7 DOWNTO 0); --! show key pressed
      dig2, 
      dig3      : OUT STD_LOGIC_VECTOR(7 DOWNTO 0)  --! debug info, current and previous key pressed
   );
END constantkey;
--------------------------------------------------------------------
ARCHITECTURE LogicFunction OF constantkey IS
  
   --! "typedef" for enum state_type.
   TYPE state_type is (
      state0_no_key,   -- Replace this state with a sensible name 
      state1_key_pressed,
      state2_key_released,-- Replace this state with a sensible name 
      state3_key_reminder,
      state4_key_reminder_2
      
   );
   
   SIGNAL current_state, 
          next_state     : state_type; -- signals of the type state_type to hold current- and next-state
   SIGNAL memory1, 
          memory2        : STD_LOGIC_VECTOR(7 DOWNTO 0); -- temporary buffers
          
BEGIN


   state_decoder: PROCESS (clk, reset) IS
   BEGIN
   
      IF (reset = '0') THEN
         current_state <= state0_no_key;
               memory1 <= (OTHERS => '0');
               dig2 <= (OTHERS => '0');
               memory2 <= (OTHERS => '0');
               dig3 <= (OTHERS => '0');
      ELSIF rising_edge(clk) THEN
         current_state <= next_state;
            IF (current_state /= next_state ) THEN
               memory1 <= scancode;
               dig2 <= scancode;
               memory2 <= memory1;
               dig3 <= memory2;
            END IF;
      END IF;

 
   END PROCESS; -- END PROCESS input_decoder

   input_decoder : PROCESS (current_state, scancode, byte_read)
   BEGIN

       CASE current_state is
      
         WHEN state0_no_key =>
            IF (byte_read = '1') THEN
               next_state <= state1_key_pressed;
            ELSE           
               next_state <= current_state;
            END IF;

         WHEN state1_key_pressed =>
            IF (byte_read = '1') AND (scancode = "11110000") THEN
               next_state <= state2_key_released;
            ELSE
               next_state <= current_state;
            END IF;
            
         WHEN state2_key_released =>
            IF (byte_read = '1') AND (scancode = "11110000") THEN
               next_state <= state3_key_reminder;
            ELSE
               next_state <= current_state;
            END IF;
            
         WHEN state3_key_reminder =>
            IF (byte_read = '0') AND (scancode = "11110000") THEN
               next_state <= current_state;
            ELSE
               next_state <= state4_key_reminder_2;
            END IF;
            
         WHEN state4_key_reminder_2 =>
            IF (byte_read = '1' ) THEN
               next_state <= current_state;
            ELSE
               next_state <= state0_no_key;               
            END IF;
            
         WHEN OTHERS =>
               next_state <= current_state;
            
      END CASE;


   END PROCESS; -- END PROCESS state_decoder


   output_decoder : PROCESS (current_state, memory1)
   BEGIN   

      CASE current_state IS
      
         WHEN state0_no_key =>
            key <= "00000000";
            
         WHEN state1_key_pressed =>
            key <= memory1;
            
         WHEN state2_key_released =>
            key <= "11110000";
            
         WHEN state3_key_reminder =>
            key <= "00000000";
            
         WHEN state4_key_reminder_2 =>
            key <= "00000000";
            
         WHEN OTHERS    =>
            key <= "00000000";
            
      END CASE;


   END PROCESS;
   
END LogicFunction;
--------------------------------------------------------------------
