--       Naam 
--      :  Joost Kuijpers 626317
--      :  Marvin Damman  616918
--      :  Mark Janssen   616931
--------------------------------------------------------------------
LIBRARY ieee;
USE     ieee.STD_LOGIC_1164.all;
USE     ieee.numeric_std.all;
--------------------------------------------------------------------
ENTITY key2pulselength is

   PORT(
      reset       : IN  STD_LOGIC;                    -- Reset active is '0'
      key         : IN  STD_LOGIC_VECTOR(7 DOWNTO 0);
      pulselength : OUT integer RANGE 0 TO 8191       -- 16 bit size to store up to maximal 56818
   );
END ENTITY key2pulselength;
--------------------------------------------------------------------
ARCHITECTURE implementation1 OF key2pulselength IS

BEGIN

   WITH key select
      pulselength <=      7102   WHEN  "00001101",   -- TAB
                          6704   WHEN  "00010110",   -- !
                          6327   WHEN  "00010101",   -- Q
                          5972   WHEN  "00011101",   -- W
                          5637   WHEN  "00100110",   -- #
                          5321   WHEN  "00100100",   -- E
                          5022   WHEN  "00100101",   -- $
                          4740   WHEN  "00101101",   -- R
                          4474   WHEN  "00101100",   -- T
                          4223   WHEN  "00110110",   -- ^
                          3986   WHEN  "00110101",   -- Y
                          3762   WHEN  "00111101",   -- &
                          3551   WHEN  "00111100",   -- U 
                          3352   WHEN  "00111110",   -- *
                          3164   WHEN  "01000011",   -- I
                          2986   WHEN  "01000100",   -- O
                          2819   WHEN  "01000101",   -- )
                          2660   WHEN  "01001101",   -- P
                          2511   WHEN  "01001110",   -- _
                          2370   WHEN  "01010100",   -- [
                          2237   WHEN  "01011011",   -- ]
                          2112   WHEN  "01100110",   -- Back
                          0      WHEN OTHERS;

   
END implementation1;
--------------------------------------------------------------------
