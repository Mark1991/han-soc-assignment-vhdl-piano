--       Naam 
--      :  Joost Kuijpers 626317
--      :  Marvin Damman  616918
--      :  Mark Janssen   616931
--------------------------------------------------------------------
LIBRARY ieee;
USE     ieee.STD_LOGIC_1164.all;
--------------------------------------------------------------------
ENTITY piano IS
   PORT(
         MAX10_CLK1_50 : IN  STD_LOGIC;                  --! 50 MHz clock on board the Cycone MAX 10 FPGA
         arduino_io4,                                    --! PS2 keyboard clock signal (20 KHz domain) to PS2_KBCLK 
         arduino_io5   : IN  STD_LOGIC;                  --! PS2 keybourd data signal (20 KHz domain) to PS2_KBDAT
         arduino_io3   : OUT STD_LOGIC;                  --! SPEAKER
         HEX0, 
         HEX1,   
         HEX2, 
         HEX3,
         HEX4,
         HEX5          : OUT STD_LOGIC_VECTOR(0 to 7);   -- 7-segment displays HEX0 to HEX3
         KEY           : IN  STD_LOGIC_VECTOR(0 to 2);   -- Switches for reset (2)
         LEDR          : OUT STD_LOGIC_VECTOR(0 to 9)
        );
END piano;
--------------------------------------------------------------------
ARCHITECTURE structure OF piano IS
   
 
   SIGNAL C_dig2 :STD_LOGIC_VECTOR(7 DOWNTO 0); 
   SIGNAL C_dig3 :STD_LOGIC_VECTOR(7 DOWNTO 0);
   SIGNAL C_Reset:STD_LOGIC;
   SIGNAL S_sw0  :STD_LOGIC_VECTOR(3 downto 0);
   SIGNAL S_sw1  :STD_LOGIC_VECTOR(3 downto 0);
   SIGNAL S_sw2  :STD_LOGIC_VECTOR(3 downto 0);
   SIGNAL S_sw3  :STD_LOGIC_VECTOR(3 downto 0);
   SIGNAL C_key  :STD_LOGIC_VECTOR(7 downto 0);


BEGIN

Display0 : work.seg7dec port map ( C_dig3(3 downto 0), HEX0 ); -- 7-segment decoder 0
Display1 : work.seg7dec port map ( C_dig3(7 downto 4), HEX1 ); -- 7-segment decoder 1
Display2 : work.seg7dec port map ( C_dig2(3 downto 0), HEX2 ); -- 7-segment decoder 2
Display3 : work.seg7dec port map ( C_dig2(7 downto 4), HEX3 ); -- 7-segment decoder 3

 L_readkey:ENTITY work.readkey PORT MAP(
      clk      =>  max10_CLK1_50,
      reset    =>  KEY(1),
      kbclock  =>  arduino_io4, 
      kbdata   =>  arduino_io5,
      key      =>  C_key,
      dig2     =>  C_dig2, 
      dig3     =>  C_dig3
 );

 L_tone_generation:ENTITY work.tone_generation PORT MAP(
      clk      =>  max10_CLK1_50,
      reset    =>  KEY(1),
      key      =>  C_key,
      audiol   =>  arduino_io3,
      audior   =>  LEDR(1)
 
);

HEX4 <= (OTHERS => '1');
HEX5 <= (OTHERS => '1');

END structure;