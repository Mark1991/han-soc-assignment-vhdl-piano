--       Naam 
--      :  Joost Kuijpers 626317
--      :  Marvin Damman  616918
--      :  Mark Janssen   616931
--------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.numeric_std.all;
--------------------------------------------------------------------
ENTITY pulselength2audio IS
   PORT( 
      reset      : IN  std_logic;                 --! Reset active is '0'
      clk_dev    : IN  std_logic;                 --! clock from programmeable clock devider
      pulslength : IN  integer RANGE 0 TO 131071; --! incoming value from key2pulselength
      audiol,
      audior     : OUT std_logic                  --! Audio outputs
   );                        
END pulselength2audio;
--------------------------------------------------------------------
ARCHITECTURE LogicFunction OF pulselength2audio IS

SIGNAL counter :  integer RANGE 0 TO 131071 := 0;
SIGNAL tone    :  STD_logic;
BEGIN

tone_generator: PROCESS (reset,clk_dev)
BEGIN
   IF (reset = '0') THEN
      tone <= '0';
      counter <= 0;
  ELSIF rising_edge(clk_dev) THEN
  
      IF (counter = pulslength) THEN
         tone <= NOT(tone);
         counter <= 0;
      ELSE
         counter  <= (counter + 1);
      END IF;
  
  END IF;
  END PROCESS tone_generator;
  
  audiol <= tone;
   
END LogicFunction;
--------------------------------------------------------------------
