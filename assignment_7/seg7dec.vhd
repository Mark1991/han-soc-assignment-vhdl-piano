--       Naam 
--      :  Joost Kuijpers 626317
--      :  Marvin Damman  616918
--      :  Mark Janssen   616931
--------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.all;
--------------------------------------------------------------------
ENTITY seg7dec IS

   port(
      C        : IN  STD_LOGIC_VECTOR(3 downto 0);
      display  : OUT STD_LOGIC_VECTOR(0 to 7) 
     
      
   );
END ENTITY seg7dec;
--------------------------------------------------------------------
ARCHITECTURE implementation OF seg7dec IS

BEGIN
   
      WITH C SELECT
         display(0 TO 6)  <= 	  	"0000001" when "0000",	-- Value 0
                                 "1001111" when "0001",	-- Value 1
                                 "0010010" when "0010",	-- Value 2
                                 "0000110" when "0011",	-- Value 3
                                 "1001100" when "0100",	-- Value 4
                                 "0100100" when "0101",	-- Value 5
                                 "0100000" when "0110",	-- Value 6
                                 "0001111" when "0111", 	-- Value 7
                                 "0000000" when "1000",	-- Value 8
                                 "0000100" when "1001",	-- Value 9
                                 "0001000" when "1010", 	-- Value A
                                 "1100000" when "1011",	-- Value B
                                 "0110001" when "1100",	-- Value C
                                 "1000010" when "1101", 	-- Value D
                                 "0110000" when "1110",	-- Value E
                                 "0111000" when "1111",	-- Value F

                                  (OTHERS =>'1') WHEN  OTHERS;
     
END ARCHITECTURE implementation;
