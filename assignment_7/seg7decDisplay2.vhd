--------------------------------------------------------------------
--! \file      seg7dec.vhd
--! \date      see top of 'Version History'
--! \brief     7-segment driver without control of dot 
--! \author    Remko Welling (WLGRW) remko.welling@han.nl
--! \copyright HAN TF ELT/ESE Arnhem 
--!
--! Version History:
--! ----------------
--!
--! Nr:    |Date:      |Author: |Remarks:
--! -------|-----------|--------|-----------------------------------
--! 001    |12-2-2015  |WLGRW   |Inital version
--! 002    |28-3-2015  |WLGRW   |Modification to set dot in display  
--! 003    |13-2-2020  |WLGRW   |Update to MAX10 on DE10-Lite board
--! 004    |31-3-2020  |WLGRW   |Modification for assignment 0-a
--! 005    |6-7-2020   |WLGRW   |Added a todo not to modify the header of the file to represent teh students that worked on the file.
--!
--! \todo MODIFY THE HEADER OF THIS FILE TO REPRESENT THE NAMES OF THE STUDENTS WORKING ON IT.
--!
--! # 7 segment decoder
--!
--! Use constants:
--! - to make the VHDL code more readable
--! - for both numbers and alphanumeric characters
--! 
--! The following constants are defined to generate alle required characters.
--! Each led in the HEX_display has been given a number for the purpose of 
--! identification. See figure 1.
--!
--! \verbatim
--!
--!  Figure 1: 7-segments lay-out:
--!  
--!        -0-
--!       |   |
--!       5   1
--!       |   |
--!        -6-
--!       |   |
--!       4   2
--!       |   |
--!        -3-  7 (dot)
--!  
--! \endverbatim
--!
--! All LEDs are grouped in a STD_LOGIC_VECTOR where the index number is
--! equal to the LED number. 
--!
--! Because the LEDs are contolled using inverted-logic we have to apply a
--! '1' to switch the LED off. 
--!
--! \todo Complete documentation
--------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.all;
--------------------------------------------------------------------
ENTITY seg7dec IS

   --! When used in the refactored 7-segement drive, 
   --! place here your CONSTANTS.

   port(
      c        : IN  STD_LOGIC_VECTOR(3 downto 0);
      display  : OUT STD_LOGIC_VECTOR(0 to 7) 
      
   );
END ENTITY seg7dec;
--------------------------------------------------------------------
ARCHITECTURE implementation OF seg7dec IS
 
BEGIN
-- start normal seg7dec code,
   display(7) <= NOT dot;
   
      WITH c SELECT
         display(0 TO 6)  <= 	  	"0000001" when "0000",	-- Value 0
                                 "1001111" when "0001",	-- Value 1
                                 "0010010" when "0010",	-- Value 2
                                 "0000110" when "0011",	-- Value 3
                                 "1001100" when "0100",	-- Value 4
                                 "0100100" when "0101",	-- Value 5
                                 "0100000" when "0110",	-- Value 6
                                 "0001111" when "0111", 	-- Value 7
                                 "0000000" when "1000",	-- Value 8
                                 "0000100" when "1001",	-- Value 9
                                 "0001000" when "1010", 	-- Value A
                                 "1100000" when "1011",	-- Value B
                                 "0110001" when "1100",	-- Value C
                                 "1000010" when "1101", 	-- Value D
                                 "0110000" when "1110",	-- Value E
                                 "0111000" when "1111",	-- Value F

                                  (OTHERS =>'1') WHEN  OTHERS;


END ARCHITECTURE implementation;
