--       Naam 
--      :  Joost Kuijpers 626317
--      :  Marvin Damman  616918
--      :  Mark Janssen   616931
--------------------------------------------------------------------
LIBRARY ieee;
USE     ieee.std_logic_1164.all,
        ieee.numeric_std.all;    -- For unsigned variable
--------------------------------------------------------------------
ENTITY showkey IS
   GENERIC(
      clk_freq              : INTEGER := 50_000_000;  --system clock frequency in Hz
      showkey_counter_size  : INTEGER := 8);          --set such that (2^size)/clk_freq = 5us (size = 8 for 50MHz)
      
      
   PORT(
      clk                   : IN  STD_LOGIC;                     -- system clock
      reset                 : IN  std_logic;                     -- reset signal active low '0'
      kbclock               : IN  std_logic;                     -- clock from keyboard in 50 MHz domain
      kbdata                : IN  std_logic;                     -- serial data from the keyboard in 50 MHz domain
      dig0,                                                      -- show key pressed on display in hex (MSB)
      dig1                  : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);  -- show key pressed on display in hex (LSB)
      scancode              : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);  -- received byte
      byte_read             : OUT std_logic                      -- '1' if byte received
      
 
   );
END showkey;
--------------------------------------------------------------------
ARCHITECTURE LogicFunction OF showkey IS

SIGNAL PS2_data   : STD_LOGIC_VECTOR (7 DOWNTO 0); 

BEGIN 

  --synchronizer flip-flops
  PROCESS(kbclock,reset, PS2_data, kbdata) -- Based on flow chart page 26 assigment reader
  
  VARIABLE counter    : INTEGER RANGE 0 to 10;
  
  BEGIN

      IF  (reset = '0') THEN
         dig0     <= x"00";
         dig1     <= x"00";
         PS2_data <= x"00";
         counter  := 0;
         byte_read<= '0';
     ELSIF (falling_edge(kbclock)) THEN
         IF (counter = 0) THEN      -- start bit
            dig0        <= ps2_data;
            byte_read   <= '0';-- as soon as the first bit from the keyboard is recieved by showkey port read_byte drops form '1' to 
            counter     := (counter + 1);
         ELSIF (counter < 9) THEN -- data bits
            PS2_data(counter-1) <= kbdata;
            counter             := (counter + 1);
         ELSIF (counter = 9) THEN -- data bits
            counter             := (counter + 1);
         ELSIF (counter = 10) THEN -- stop bits
            byte_read <= '1'; --scancode is complete, byte_read will shift to 1
            dig1      <= PS2_data; -- msb > LSB shift
            scancode  <= PS2_data; -- msb > LSB shift 
            counter   := 0;
         END IF;
     END IF;
   END PROCESS;



  
end LogicFunction;
--------------------------------------------------------------------

