--       Naam 
--      :  Joost Kuijpers 626317
--      :  Marvin Damman  616918
--      :  Mark Janssen   616931
--------------------------------------------------------------------
LIBRARY ieee;
USE     ieee.STD_LOGIC_1164.all;
USE     ieee.STD_LOGIC_arith.all;
USE     ieee.STD_LOGIC_unsigned.all;
--------------------------------------------------------------------
ENTITY tone_generation IS
   PORT(
      clk     : IN  STD_LOGIC;                     -- 50 MHz clock.
      reset   : IN  STD_LOGIC;                     -- reset '0' active
      key     : IN  STD_LOGIC_VECTOR(7 downto 0);  -- hex character received from keyboard
      audiol,
      audior  : OUT STD_LOGIC                      -- Audio output
   );
END tone_generation;
--------------------------------------------------------------------
ARCHITECTURE tone_generation_struct OF tone_generation IS
   
   SIGNAL   c_clk_div     :  STD_LOGIC;
   SIGNAL   c_clk_dev     :  STD_LOGIC; 
   SIGNAL   C_pulselength :  integer RANGE 0 TO 131071;
   
BEGIN
   
 L_clock_generator:ENTITY work.clock_generator PORT MAP(

 reset => reset,
 clk => clk, 
 key => key,
 clk_div => C_clk_div
 ); 
 

 L_key_2_pulselength: ENTITY work.key2pulselength PORT MAP(
 reset   => reset,     
 key     => key,
 pulselength => C_pulselength
 );


 L_pulselength_2_audio: ENTITY work.pulselength2audio PORT MAP( 
 reset      => reset,    
 clk_dev    => C_clk_div,
 pulslength => C_pulselength,
 audiol     => audiol,
 audior     => audior
 ); 
   
END tone_generation_struct;

